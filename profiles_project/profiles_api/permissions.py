from rest_framework import permissions

class UpdateProfileUserPermissions(permissions.BasePermission):
    """Allow user to edit the own profile"""

    def has_object_permission(self, request, view, obj):
        """Check user is trying edit to own profile"""
        if request.method in permissions.SAFE_METHODS:
            return True
            
        return obj.id == request.user.id

        
