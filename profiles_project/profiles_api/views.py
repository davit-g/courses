from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import *
from rest_framework import settings, viewsets
from .models import *
from rest_framework.authentication import TokenAuthentication
from .permissions import *
from rest_framework import filters
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

class HelloApiView(APIView):
    serializer_class = HelloSerializer

    def get(self, request, format = None):

        as_apiview = [
            'This is api view',
            'APIView is default views to response'
        ]
        return Response({
            'message':'Hello!!!',
            'detail': as_apiview
        })

    def post(self, request, format = None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f'Hello {name}'
            return Response({
                'message':message
            })
        else:
             return Response ({
                 serializer.errors,
             })  

class HelloViewset(viewsets.ViewSet):
    """This is ViewSet"""
    serializer_class = HelloSerializer
    def list(self, request):
        ai_viewset = [
            'This is test viewset',
            'Viewset is very easy'
        ]
        return Response({
            'message':ai_viewset
        })

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f'Hello {name}!!!'
            return Response({
                'message':message
            })
        else:
             return Response ({
                 serializer.errors,
             })  
    
    def retrieve(self, request, pk = None):
        return Response({
            'http_method':'GET'
        })
    def update(self, request, pk = None):
        return Response({
            'http_method':'PUT'
        })
    def partial_update(self, request, pk = None):
        return Response({
            'http_method':'Patch'
        })
    def destroy(self, request, pk = None):
        return Response({
            'http_method':'DELETE'
        })
class UserProfileViewSet(viewsets.ModelViewSet):
    """This is ModelViewSet to manage UserProfile, not APIView or ViewSet, only ModelViewSet"""
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes  = (UpdateProfileUserPermissions, )
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'email',)



class UserLoginApiView(ObtainAuthToken):
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
    

class UserProfileFeedViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, )
    serializer_class = ProfileFeedSerializer
    queryset = ProfileFeedItem.objects.all()


    def perform_create(self, serializer):
        serializer.save(user_profile = self.request.user)
        